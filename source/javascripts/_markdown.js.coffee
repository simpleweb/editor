# Mechanism to load and save markdown content from tinymce
# Based on https://github.com/donShakespeare/twExoticMarkdownEditor/

# Regexp to find the eventual meta data
separator = /^(---|\+\+\+)$/m

# Meta display enveloppe
preStart = '<pre class="meta" style="background-color: #cde; box-shadow: 0 0 1em #eee; max-width: 60em; margin: 0 auto; padding: 0 0.3em; margin-top: -0.6em; white-space: pre-wrap;">'
preEnd = '</pre>'

# Save content to file
window.saveMarkdown = (content) =>
	content = content
		.replace(/(?:<br>|<br \/>)/g, '\n')
		.replace(preStart, '')
		.replace(preEnd, '')

	splits = content.split separator

	if splits.length >= 5
		# Suppress html elements and only get the text
		head = splits[1] + $('<div>').html(splits[2]).text() + splits[1]
		# Remove extra spaces
		head = head
			.replace(/\s+$/gm, '')
			.replace(/\u00A0/gm, ' ')
			# Help for the : placement
			.replace(/\b\s*:\s*\b/g, ': ')
		# Escape another eventual colon, using "
		if /title:\s+[^"].*:.*[^"]/.test head
			head = head.replace /title:\s+(.*)/, "title: \"$1\""
		body = splits[4]

	else
		head = ''
		body = content

	head + body
		.replace(/(\s)\s+/g, '$1')
		.replace(/(?:<\/p>)/g, '\n')
		.replace(/(?:<p>\s*)/g, '')
		.replace(/(&nbsp;)/g, ' ')
		.replace(/(?:&amp;gt;|&gt;)/g, '>')

# Load content from file
window.loadMarkdown = (content) =>
	splits = content?.split separator

	if splits?.length >= 5
		head = [preStart, splits[2], preEnd].join splits[1]
		body = splits[4]

	else
		head = ''
		body = content

	return unless content

	head + body
		.replace(/  /g, ' &nbsp;')
		.replace(/(?:\r\n|\r|\n){2}/g, '<p>')
		.replace(/([^>])(?:\r\n|\r|\n)/g, '$1<br />')
		.replace(/(?:&amp;gt;|&gt;)/g, '>')
		.replace(/(?:&amp;)/g, '&')
		.replace(/(?:<p>)<(p|h1|h2|h3|h4|h5|h6|hr|figure|img|ul|ol|dl)/g, '<$1')
