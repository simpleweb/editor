# Setup ace editor
window.setupAce = ->
	$('textarea#filecontent').hide() # Hide eventual tinymce basic textarea
	$('#editor, form#edit button').show()

	editor = ace.edit 'editor'
	editor.setValue $('form#edit textarea#filecontent').val()
	# To deselect all content
	editor.gotoLine 1
	document.getElementById('editor').style.fontSize = 'inherit'

	# Get file type, to activate proper ace mode
	type = $('form#edit textarea#filecontent')
		.data 'content-type'
		.replace /.*\//, ''
	editor.setOptions
		mode: "ace/mode/#{type}"
		wrap: true

	editor.on 'change', (e) ->
		$('form#edit textarea#filecontent').val editor.getValue()
